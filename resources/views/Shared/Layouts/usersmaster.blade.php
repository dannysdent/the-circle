<!DOCTYPE html>

<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title','The Circle')</title>
    <meta name="description" content="Ticketing Platform">
    <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/styles-merged.css">
    <link rel="stylesheet" href="/css/style.min.css">
    <link rel="stylesheet" href="/css/custom.css">

    <script src="/js/scripts.min.js"></script>
    <script src="/js/main.min.js"></script>
    <script src="/js/custom.js"></script>
    @yield('additionalscripts')
  </head>

  <body>

    @section('navigation')
      <header role="banner" class="probootstrap-header">
        <!-- <div class="container"> -->
          <div class="row">
            <a href="/homepage" class="probootstrap-logo visible-xs"><img src="/img/logo_sm.png" class="hires" width="120" height="33" alt="Free Bootstrap Template by uicookies.com"></a>

            <a href="#" class="probootstrap-burger-menu visible-xs"><i>Menu</i></a>
            <div class="mobile-menu-overlay"></div>

            <nav role="navigation" class="probootstrap-nav hidden-xs">
              <ul class="probootstrap-main-nav">
                <li class="active"><a href="/homepage">Home</a></li>
                <li><a href="#">This Weeknd</a></li>
                <li><a href="#">Upcoming</a></li>
                <li class="hidden-xs probootstrap-logo-center"><a href="/homepage"><img src="/img/thecircle2.jpg" class="hires" width="181" height="50" alt="The Circle"></a></li>
                <li><a href="#">Terms & Conditions</a></li>
                <li><a href="#">Support</a></li>
              </ul>
              <div class="extra-text visible-xs">
                <a href="#" class="probootstrap-burger-menu"><i>Menu</i></a>
                <h5>Connect With Us</h5>
                <ul class="social-buttons">
                  <li><a href="#"><i class="icon-twitter"></i></a></li>
                  <li><a href="#"><i class="icon-facebook2"></i></a></li>
                  <li><a href="#"><i class="icon-instagram2"></i></a></li>
                </ul>
              </div>
            </nav>
          </div>
        <!-- </div> -->
      </header>
    @show

    @yield('bodycontent')

    @section('contentBeforeFooter')
      <section class="probootstrap-half">
        <div class="image" style="background-image: url(img/slider_2.jpg);"></div>
        <div class="text">
          <div class="probootstrap-animate fadeInUp probootstrap-animated">
            <h2 class="mt0">About The Circle</h2>
            <p><img src="/img/curve_white.svg" class="seperator" alt="Free HTML5 Bootstrap Template"></p>
            <div class="row">
              <div class="col-md-6">
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
              </div>
              <div class="col-md-6">
                <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
              </div>
            </div>
          </div>
        </div>
      </section>

    @show

    @section('footer')
      <!-- START: footer -->
      <footer role="contentinfo" class="probootstrap-footer">
        <div class="container">
          <div class="row mt40">
            <div class="col-md-12 text-center">
              <ul class="probootstrap-footer-social">
                <li><a href=""><i class="icon-twitter"></i></a></li>
                <li><a href=""><i class="icon-facebook"></i></a></li>
                <li><a href=""><i class="icon-instagram2"></i></a></li>
              </ul>
              <p>
                <small>&copy; 2019 <a href="https://uicookies.com/" target="_blank">Trusted & Eazzy</a>. All Rights Reserved.</small>
              </p>

            </div>
          </div>
        </div>
      </footer>
      <!-- END: footer -->
    @show
  </body>
</html>
