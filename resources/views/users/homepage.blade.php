@extends('Shared.Layouts.usersmaster')


@section('title','The Circle')

@section('bodycontent')
  @include('users.partials.highlightimage')
  <section class="probootstrap-section">
    <div class="container">
      <div class="row mb30">
        <div class="col-md-8 col-md-offset-2 probootstrap-section-heading text-center">
          <h2>How it works</h2>
          <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
          <p><img src="/img/curve.svg" class="svg" alt="Free HTML5 Bootstrap Template"></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="service left-icon probootstrap-animate">
            <div class="icon">
              <i class="icon-calendar3"></i>
            </div>
            <div class="text">
              <h3>Pick Event</h3>
              <p>Choose from a variety of events from The Circle Platform .the vents vary from date and the location that will be held.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="service left-icon probootstrap-animate">
            <div class="icon">
              <i class="icon-wallet2"></i>
            </div>
            <div class="text">
              <h3>Make Payment</h3>
              <p>Various payment options are available for non kenyan citizens and kenyan citizens.Make sure you have the correct details entered before proceeding to checkout.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="service left-icon probootstrap-animate">
            <div class="icon">
              <i class="icon-user-check"></i>
            </div>
            <div class="text">
              <h3>Confirmation</h3>
              <p>After your payment has been confirmed, your ticket will be automatically generated and you can be able to print it and showcase it while attending the event.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="probootstrap-section probootstrap-section-dark">
    <div class="container">
      <div class="row mb30">
        <div class="col-md-8 col-md-offset-2 probootstrap-section-heading text-center">
          <h2>Highlights</h2>
          <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
          <p><img src="/img/curve.svg" class="svg" alt="Free HTML5 Bootstrap Template"></p>
        </div>
      </div>
      <div class="row probootstrap-gutter10">
        @if($data->count() ==0)
          <h4 style="text-transform:uppercase;"> No events found </h4>
        @else
          @php
            $count=0;
          @endphp

          @foreach ($data as $eventdata)
            @if(++$count >2)
              @break
            @endif

            <div class="col-md-6">
              <div class="probootstrap-block-image-text">
                <figure>

                  <a href="img/img_1.jpg" class="image-popup">
                    @php
                        $eventimage=DB::table('event_images')->where('event_id',$eventdata->id)->get();
                        if($eventimage->count()==0)
                        {$path='';}
                        else
                        {
                          $eventimage=$eventimage->first();
                          $path = $eventimage->image_path;
                        }
                    @endphp

                    <img src="{{$path}}" alt="Event Image not found" class="img-responsive">

                  </a>
                  <div class="actions">
                    <a href="https://vimeo.com/45830194" class="popup-vimeo"><i class="icon-play2"></i></a>
                  </div>
                </figure>
                <div class="text">
                  <h3><a href="#">{{$eventdata->title}}</a></h3>
                  <div class="post-meta">
                    <ul>
                      <li><span class="review-rate">4.7</span> <i class="icon-star"></i> 252 Reviews</li>
                      <li><i class="icon-user2"></i> 3 Sold</li>
                    </ul>
                  </div>
                  <p>{{substr($eventdata->description,0,100)."..."}}</p>
                  <p><a href="next/{{$eventdata->id}}" class="btn btn-primary">Purchase Ticket</a></p>
                </div>
              </div>
            </div>

          @endforeach
        @endif
      </div>
    </div>
  </section>
  <section class="probootstrap-section">
    <div class="container">
        <div class="row">
          <div class="col-md-12 probootstrap-relative">
            <h3 class="mt0 mb30">Upcoming Events</h3>
            <ul class="probootstrap-owl-navigation absolute right">
              <li><a href="#" class="probootstrap-owl-prev"><i class="icon-chevron-thin-left"></i></a></li>
              <li><a href="#" class="probootstrap-owl-next"><i class="icon-chevron-thin-right"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 probootstrap-relative">
            <div class="owl-carousel owl-carousel-carousel">
             @php
                $eventcount=$data->count();
                $count=0;
             @endphp

             @if($eventcount ==0)
                <h4 style="text-transform:uppercase;"> No events found </h4>
             @else
               @foreach($data as $eventdata)
                  @php
                  
                    $eventimage=DB::table('event_images')->where('event_id',$eventdata->id)->get();
                    if($eventimage->count()==0)
                    {$path='';}
                    else
                    {
                      $eventimage=$eventimage->first();
                      $path = $eventimage->image_path;
                    }

                  @endphp

                  @if($count>10 || $count==$eventcount)
                    @break
                  @endif

                  <div class="item">
                    <div class="probootstrap-room">
                      <a href="#"><img src="{{$path}}" alt="Event Image not found" class="img-responsive"></a>
                      <div class="text">
                        <h3>{{$eventdata->title}}</h3>
                        <p>Starting from <strong>Ksh 2,900</strong></p>
                        <div class="post-meta">
                          <ul>
                            <li><span class="review-rate">4.7</span> <i class="icon-star"></i> 252 Reviews</li>
                            <li><i class="icon-user2"></i> 3 Sold</li>
                            <p><a href="next/{{$eventdata->id}}" class="btn btn-primary">Purchase Ticket</a></p>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  @php
                    ++$count;
                  @endphp
                @endforeach
                 {{--end of loop--}}
              @endif

            </div>
          </div>
        </div>
      </div>
  </section>   

@endsection
