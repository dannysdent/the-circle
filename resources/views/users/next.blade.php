@extends('Shared.Layouts.usersmaster')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js">
  
</script>
 --}} {{--DON'T KNOW WHAT EFFECT IT BRINGS ON OR NOT--}}

@section('title','Ticket Purchase')

@section('additionalscripts')
  <script type="text/javascript">
    var current_date=new Date();
    var event_date=new Date('{{$data->start_date}}');
    var converttodays=1000*60*60*24;
    var timedifference=(event_date-current_date)/converttodays


    $(function()
    {
        $('#value1, #value2').on('keyup keydown mouseup mousedown',function()
        {
           const DISCOUNTDAYS=7; //get discount if you buy before 7 days
           var value1 = parseFloat($('#value1').val()) || 0; //adultquantity
           var value2 = parseFloat($('#value2').val()) || 0; //childquantity

           var totaltickets = value1+value2;
           var totalprice=value1*{{$price}} +value2;

           if (totaltickets>3 && totaltickets<=10 )
           {totalprice*=0.80;} //20% discount

           if (totaltickets>10)
           {totalprice*=0.70;} //30% discount


           if(timedifference>=7) //early bird
           {totalprice*=0.80;}//20% discount

           $('#sum').val(Math.round(totalprice));
        });

    });

  </script>

@endsection
                 
@section('bodycontent')
  <section class="probootstrap-section">
    <div class="container">
      <div class="row probootstrap-gutter60">
        <div class="col-md-6">
          <img width="512" height="512" src="{{asset($eventimage)}}" >
        </div>
        <div class="col-md-6">
          <p><b>Runs till:</b> {{$data->end_date}}</p>
          <p><b>Time:</b> 08:00 AM - 10:00 PM</p>
          <p><b>Location:</b> {{$data->venue_name}}</p>
          <p><b>Host :</b> {{$host}}</p>
          <p>{{$data->description}}</p>

          </div>
          <form>

            @if($filter) 

           <div class="row mb30">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="adults">Adults. Unit cost: Ksh {{$price}}</label>
                  <div class="form-field">

                        <input type="number" name="value1" id="value1" class="form-control" min="0" placeholder="Enter first value" required />

                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label for="children">Total. Ksh</label>
                  <div class="form-field">

                        <input type="number" name="sum" id="sum" class="form-control" readonly />
                  </div>
                </div>
              </div>
              <br>
          </form>

          
          <p>
            <a href="/details/confirm/{{$data->id}}#confirm_details" class="btn btn-primary" role="button">Proceed To Pay</a>
          </p>
        </div>

        @else


        <p>Ticket purchase is currently unavailable</p>
        @endif
        </div>
      </div>
    </div>
  </section>

@endsection

