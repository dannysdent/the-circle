<head>
    <style>
        table
        {
             border: 1px solid {{$event->ticket_border_color}};
             background-color:{{$event->ticket_bg_color}};
             color: {{$event->ticket_sub_text_color}};
             empty-cells: show;
             border-collapse: collapse;
             position:fixed;
             top:10px;
             left:200px;
        }
        td
        {
            border:none;
            padding: 7px;
        }

        td h3
        {
            color: {{$event->ticket_text_color}};
        }

        #ticketimage
        {
            position:relative;
            top:-140px;
            left:5px;
            width:100px;
            height:100px;

            border-left: 1px solid {{$event->ticket_border_color}};
            border-bottom: 1px solid {{$event->ticket_border_color}};
        }
        #qrposition
        {
            position:relative;
            bottom:-120px;
            left:-6px;
            border: 1px solid {{$event->ticket_bg_color}};
        }
    </style>
</head>

<body>
    @php

        $imagepath=public_path().'/'.$imagearr['eventimage'];
        $ticketuniqueid=uniqid();
    @endphp
    <table>
        <tbody>
            <tr>
                <td rowspan="5">
                    <div class="barcode">
                       {{-- {!! QrCode::size(250)->generate($ticketuniqueid)->format('png'); !!} --}}
                       {{-- {!! QrCode::format('png')->generate($ticketuniqueid); !!} --}}
                       {{-- <img src="{!!QrCode::format('png')->generate(($ticketuniqueid),public_path('img/qrcode.png'))!!}"> --}}
                       @php
                        //convert the hex colour to rgb and then we can go from there,
                        //we use hexdec(), i.e hexdec('ff') //it converts to decimal
                        //#ffffff
                        //colour of the qr
                        $qrred=hexdec(substr($event->ticket_text_color,1,2));
                        $qrgreen=hexdec(substr($event->ticket_text_color,3,2));
                        $qrblue=hexdec(substr($event->ticket_text_color,5,2));
                        //colour for the background of the qr
                        $bred=hexdec(substr($event->ticket_bg_color,1,2));
                        $bgreen=hexdec(substr($event->ticket_bg_color,3,2));
                        $bblue=hexdec(substr($event->ticket_bg_color,5,2));

                        //qr code generation
                        QrCode::format('png')->color($qrred,$qrgreen,$qrblue)
                        ->backgroundColor($bred,$bgreen,$bblue)
                        ->size(150)->generate(($ticketuniqueid),public_path('img/qrcode.png'));
                       @endphp

                       <img src="{{public_path('img/qrcode.png')}}" id="qrposition">


                    </div>
                    @if($event->is_1d_barcode_enabled)
                        <div class="barcode_vertical">
                            {!! DNS1D::getBarcodeSVG(12211221, "C39+", 1, 50) !!}
                        </div>
                    @endif
                </td>
                <td> 
                    <h3>EVENT</h3>
                    <p>{{$event->title}}</p>
                </td>
                <td> 
                    <h3>NAME </h3>
                    <p>{{$ticket->title}}</p>
                </td>
                <td rowspan="5">
                     @if($imagearr['eventimage']) 
                        {{--only run if an image for the ticket exists--}}
                        <img id="ticketimage" src="{{$imagepath}}">
                     @endif
                </td>               
            </tr>
             <tr>
                <td> 
                    <h3>ORGANISER</h3> 
                    <p>{{$organiser->name}}</p>
                </td>
                <td> 
                    <h3>TICKET TYPE</h3>
                    <p>Ticket type</p>
                 </td>             
            </tr>
             <tr>
                <td> 
                    <h3>VENUE</h3>
                    <p>{{$event->venue_name}}</p>
                </td>
                <td> 
                    <h3>ORDER REF</h3>
                    <p>{{$ticketuniqueid}}</p>
                </td>           
            </tr>
             <tr>
                <td> 
                    <h3>START DATE/ TIME</h3> 
                    <p>{{$event->start_date}}</p>
                </td>
                <td> 
                    <h3>ATTENDEE REF</h3> 
                    <p>Attendee ref</p>
                </td>              
            </tr>
             <tr>
                <td> 
                    <h3>END DATE/ TIME</h3>
                    <p>{{$event->end_date}}</p>

                </td>

                <td> 
                    <h3>PRICE</h3>
                    <p>Ksh. {{$ticket->price}}</p>
                </td>             
            </tr>
        </tbody>
    </table>

</body>
 