@extends('Shared.Layouts.usersmaster')

@section('title','Details Confirmation')

@section('bodycontent')
   @include('users.partials.highlightimage')
  {{-- <section class="probootstrap-slider flexslider probootstrap-inner">
    <ul class="slides">
       <li style="background-image: url(/img/slider_1.jpg);" class="overlay">
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="probootstrap-slider-text text-center">
                  <p><img src="/img/curve_white.svg" class="seperator probootstrap-animate" alt="Free HTML5 Bootstrap Template"></p>
                  <h1 class="probootstrap-heading probootstrap-animate">Confirm Details</h1>
                </div>
              </div>
            </div>
          </div>
        </li>
    </ul>
  </section> --}}

  <h1 class="probootstrap-heading probootstrap-animate" id="confirm_details" style="text-align:center;">
   Confirm Details
  </h1>

  <section class="probootstrap-section">
    <div class="container">
      <div class="row probootstrap-gutter40">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
          <form action="#" method="post" class="probootstrap-form">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="fname">First Name</label>
                  <input type="text" placeholder="John" class="form-control" id="fname" name="fname">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="lname">Last Name</label>
                  <input type="text" placeholder="Doe" class="form-control" id="lname" name="lname">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="email">Email</label>
              <div class="form-field">
                <i class="icon icon-mail"></i>
                <input type="email" class="form-control" placeholder="me@mail.com" id="email" name="email">
              </div>
            </div>
            <div class="form-group">
              <label for="email">Phone Number</label>
              <div class="form-field">
                <i class="icon icon-phone2"></i>
                <input type="email" placeholder="254700000000" class="form-control" id="email" name="email">
              </div>
            </div>
            <p>
              <a href="{{route("ticket_generation",["event_id"=>$eventid])}}" class="btn btn-primary" role="button">Proceed</a>
            </p>
          </form>
        </div>
        <div class="col-md-2">
        </div>
      </div>
    </div>
  </section>

@endsection



  

