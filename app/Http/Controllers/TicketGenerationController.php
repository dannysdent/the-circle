<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events;
use App\EventImages;
use App\Eventorganiser;
use App\Ticket;
use PDF;

class TicketGenerationController extends Controller
{
	public function ticketGeneration($event_id)
	{
		$event=Events::where('id',$event_id)->get()->first(); //guaranteed the ticket is there
		$eventimage=EventImages::where('event_id',$event_id)->get();
		$ticket=Ticket::where('event_id',$event_id)->get()->first(); //the ticket is guaranteed to be there
		$organiser=Eventorganiser::where('id',$event->organiser_id)->get()->first(); //get the organiser
        if ($eventimage->count()==0)
        {$imagearr['eventimage']=false;} //no event
        else
        {$imagearr['eventimage']=$eventimage->first()->image_path;} //get the path to the image
		// return view('users.ticketgeneration',[
		// 				'event'=>$event,
		// 				'imagearr'=>$imagearr,
		// 				'ticket'=>$ticket,
		// 				'organiser'=>$organiser,
		// 			]);
        $pdf=PDF::loadView('users.ticketgeneration',compact('event','imagearr','ticket','organiser'));
        return $pdf->download('ticket.pdf');
	}
}
