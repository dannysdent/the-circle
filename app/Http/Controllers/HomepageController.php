<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Events;
use App\EventImages;
use App\Eventorganiser;
use App\Ticket;


class HomepageController extends Controller
{
	private $data,$eventimage,$organiser;

	public function queryData()
	{
		//from this function we query the data from the database
		$this->data=Event::all();
     	$this->eventimage=EventImages::all();
		
	}

    public function showHomepage()
    {
    	$data=Events::all();
        $data=$data->sortBy('start_date'); //we sort the events by their start date
        //return strtotime($data->start_date)." ".strtotime($data->end_date);
    	return view('users.homepage',['data'=>$data]);
    }

    public function showNext($id)
    {
    	$data=Events::where('id',$id)->get()->first(); //guaranteed the data is there
    	$eventimages=EventImages::where('event_id',$id)->get();
        if ($eventimages->count()==0)
        {$eventimages="no image";}
        else
        {$eventimages=$eventimages->first()->image_path;}

        //guaranteed that an event has an organiser.
    	$host=Eventorganiser::where('id',$data->organiser_id)->get()->first()->name;        

    	$ticketprice=Ticket::where('event_id',$id)->get();
        if ($ticketprice->count()==0 || $ticketprice->first()->is_paused == 1)
        {
            $ticketprice=0;
            $filtervar=false;
        } //if no ticket is found, set it's price to 0
        else //get the price of the ticket
        {
           $ticketprice=$ticketprice->first()->price;
           $filtervar=true;
        }

    	return view('users.next',[
    		'data'=>$data,
    		'eventimage'=>$eventimages,
    		'host'=>$host,
    		'price'=>$ticketprice,
            'filter'=>$filtervar,
    	]);
    }



}
