<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    public function paymentConfirmation($event_id)
    {
    	return view('users.confirm',['eventid'=>$event_id]);
    }

    public function showPayment()
    {
    	return view('users.payment');
    }

}
