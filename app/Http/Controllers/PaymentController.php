<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaymentController extends Controller
{
    //
    public function paymentTrial()
    {
    	header("Content-Type:application/json");
    	//get the response from mpesa //-- response is a json
    	$myresponse['ResultCode']=1; //for a failure

    	$mpesaresponse=file_get_contents("php://input");
    	return $mpesaresponse;

    	if($mpesaresponse)
    	{
	    	$content=json_decode($mpesaresponse);

	    	if ($content->RequestType === "confirm")
	    	{$myresponse['ResultCode']=confirmPayment();}
	    	else if ($content->RequestType === "validate")
	    	{$myresponse['ResultCode']=validatePayment();}
		}
    	
    	return $myresponse;
    }

    public function confirmPayment()
    {
    	//agree for the application to receive the payment

    	return 0; //for success
    	//return 1; //for a failure
    }

    public function validatePayment()
    {
    	//after mpesa has processed the payment.
    	//do some processing e.g. updating the database
    	return 0; //for success
    	//return 1; //for a failure.
    }

}
 